package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.listener.LoggerListener;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;


public final class Bootstrap {

    @SneakyThrows
    public void init() {
        @NotNull final ActiveMQConnectionFactory connectionFactory =
            new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_URL);
        connectionFactory.setTrustAllPackages(true);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue("TM_QUEUE");
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(new LoggerListener());
    }

}
